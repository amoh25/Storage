import datetime
import json

import connexion
from connexion import NoContent
import swagger_ui_bundle

import mysql.connector 
import pymysql
import yaml
import logging
import logging.config

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from base import Base
from buy import Buy
from sell import Sell

import pykafka
from pykafka import KafkaClient
from pykafka.common import OffsetType

import threading
from threading import Thread

with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

with open('storage-app_conf.yml', 'r') as f:
    conf = yaml.safe_load(f.read())

DB_ENGINE = create_engine(f"mysql+pymysql://{app_config['user']}:{app_config['password']}@{app_config['hostname']}:{app_config['port']}/{app_config['db']}")
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)

def process_messages():
    client = KafkaClient(hosts=f"{conf['events']['hostname']}:{conf['events']['port']}")
    topic = client.topics[conf['events']['topic']]
    messages = topic.get_simple_consumer(
        reset_offset_on_start = False,
        auto_offset_reset = OffsetType.LATEST
    )

    for msg in messages:
        msg_str = msg.decode('utf-8')
        msg = json.loads(msg_str)
        payload = msg['payload']
        type = msg['type']
        session = DB_SESSION()
        logger.info("CONSUMER::storing buy event")
        logger.info(msg)
        if type == 'buy':
            obj = Buy(
                sell_id = payload['buy_id'],
                item_name = payload['item_name'],
                item_price = payload['item_price'],
                buy_qty = payload['buy_qty'],
                trace_id = payload['trace_id']
            )
        else:
            obj = Sell(sell_id=payload['sell_id'],
                       item_name=payload['item_name'],
                       item_price=payload['item_price'],
                       sell_qty=payload['sell_qty'],
                       trace_id=payload['trace_id'])
        session.add(obj)
        session.commit()
    messages.commit_offsets()
# Endpoints
def buy(body):
    session = DB_SESSION()
    buy = Buy(buy_id=body['buy_id'], item_name=body['item_name'] ,item_price=body['item_price'], buy_qty=body['buy_qty'], trace_id=body['trace_id'])
    session.add(buy)
    session.commit()
    return NoContent, 201

def get_buys(timestamp):
    data = []
    with DB_SESSION.begin() as session:
        rows = session.query(Buy).filter(Buy.date_created >= timestamp)
        for row in rows:
            data.append(row.to_dict())
    logging.info("get_buys: timestamp: %s, number of results: %s", timestamp, len(data))
    return data, 200
def sell(body):
    session = DB_SESSION()
    sell = Sell(sell_id=body['sell_id'], item_name=body['item_name'] ,item_price=body['item_price'], sell_qty=body['sell_qty'], trace_id=body['trace_id'])
    session.add(sell)
    session.commit()

    return NoContent, 201

def get_sells(timestamp):
    session = DB_SESSION()
    rows = session.query(Sell).filter(Sell.date_created >= timestamp)
    data = []
    for row in rows:
        data.append(row.to_dict())
    session.close()
    logging.info(f"get_sells: timestamp: {timestamp}, number of results: {len(data)}")
    return data, 200
app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api('openapi.yaml', base_path="/storage" ,strict_validation=True, validate_responses=True)

with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basic')

if __name__ == "__main__":
    tl = Thread(target=process_messages)
    tl.daemon = True
    tl.start()
    app.run(port=8090)
